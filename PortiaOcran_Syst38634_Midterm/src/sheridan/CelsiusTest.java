//PORTIA OCRAN
package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class CelsiusTest {

	@Test
	public void testFromFahrenheitRegular() {
		int celcius = Celsius.fromFahrenheit(10);
		assertTrue("Incorrect Farenheit from celsius.", celcius == -12);
	}
	
	@Test
	public void testFromFarenheitBoundaryIn() {
		int celcius = Celsius.fromFahrenheit(1);
		int celcius2 = Celsius.fromFahrenheit(2);
		assertTrue("Two seperate temperatures should not produce same result", celcius != celcius2);
	}
	
	@Test
	public void testFromFarenheitBoundaryOut() {
		int celcius = Celsius.fromFahrenheit(54);
		int celcius2 = Celsius.fromFahrenheit(55);
		assertFalse("Two seperate temperatures should not produce same result", celcius != celcius2);
	}
	
	@Test
	public void testFromFarenheitException() {
		int celcius = Celsius.fromFahrenheit(-459);
		
		assertFalse("-273 is lowest possible temp- absoolute 0.", celcius < -273);
	}

}
