package sheridan;

public class Celsius {
	
	public static int fromFahrenheit(int fahrenheit) {
		
		
		int celcius = (int) (Math.ceil(( 5 *(fahrenheit - 32.0)) / 9.0));
		
		return celcius;
	}

}
